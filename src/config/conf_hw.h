/**
 * @author: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 * @date: 29.11.2016
 * @defgroup hw_conf
 * Hardware configuration.
 */

/**
 * @file
 * @ingroup hw_conf
 */

#ifndef HW_CONF_H
#define HW_CONF_H

#define HSI_HZ                  16000000U   //!< High speed clock frequency
#define PCLK1_HZ                HSI_HZ      //!< Periph clock speed
#define CLK_APB_MHZ             16U         //!< Periph bus speed in MHz
#define MHZ                     1000000U    //!< 1MHz in Hz
#define TIMER_BASE_FREQ         1000U       //!< Timer base frequency [Hz]

#endif //HW_CONF_H
