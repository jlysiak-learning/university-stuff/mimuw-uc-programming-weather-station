/**
 * @defgroup main
 * @author: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 */
/**
 * @file
 * @ingroup main
 * @date: 07.01.2016
 */
//--- HARDWARE STUFF & CONFIG
#include <conf_hw.h>
//--- IMPORTANT MODULES
#include <timer.h>
#include <i2c.h>
#include <lcd.h>
//--- ON-BOARD SENSORS
#include <hts221.h>
#include <lps331ap.h>
#include <stlm75.h>
#include <tsl2572.h>
//--- STDLIB
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
/*-----------------------------------  TIMER  --------------------------------------*/

static volatile uint32_t up_time;   //!< Up time in seconds

/**
 * System uptime timer.
 * Increments counter every second and start measurements.
 */
static void up_timer(uint16_t r) {
    up_time++;
    /* Start measurements */
    stlm75_measure();
    hts221_measure();
    lps331ap_measure();
}
/*------------------------------------  LCD  --------------------------------------*/

#define LCD_BUFF_SIZE   64              //!< LCD buffer length
static char lcd_buff[LCD_BUFF_SIZE];    //!< LCD text buffer

/** Send C-like string to LCD */
static void lcd_send_string(const char * str) {
    while(*str)
        LCDputcharWrap(*str++);
}

/** Prepare LCD layout. */
static void lcd_layout(void) {
    LCDgoto(0, 0);
    sprintf(lcd_buff, "HTS221\n");  // line: 0
    lcd_send_string(lcd_buff);
    sprintf(lcd_buff, " rH: \n");   // line: 1
    lcd_send_string(lcd_buff);
    sprintf(lcd_buff, " T: \n");    // line: 2
    lcd_send_string(lcd_buff);
    sprintf(lcd_buff, "LPS331\n");  // line: 3
    lcd_send_string(lcd_buff);
    sprintf(lcd_buff, " P: \n");    // line: 4
    lcd_send_string(lcd_buff);
    sprintf(lcd_buff, " T: \n");    // line: 5
    lcd_send_string(lcd_buff);
    sprintf(lcd_buff, "STLM75\n");  // line: 6
    lcd_send_string(lcd_buff);
    sprintf(lcd_buff, " T: \n");    // line: 7
    lcd_send_string(lcd_buff);
    sprintf(lcd_buff, "TSL2572\n"); // line: 8
    lcd_send_string(lcd_buff);
    sprintf(lcd_buff, " L: ");      // line: 9
    lcd_send_string(lcd_buff);
}
/*-------------------------  SYSTEM STATUS REGISTER  -------------------------------*/

#define STATUS_RDY_LPS331       0x01    //!< LPS331 data ready
#define STATUS_RDY_HTS221       0x02    //!< HTS221 data ready
#define STATUS_RDY_STLM75       0x04    //!< STLM75 data ready
#define STATUS_RDY_TSL2572      0x08    //!< TSL2572 data ready

static volatile uint8_t status; //!< System status register
/*------------------------ SENSORS DATA READY CALLBACKS ----------------------------*/
/* Each of callback sets proper flag in system status register. Flags are checked
 * in main loop. Data update cause refresh corresponding part of LCD.               */

void lps331ap_cb(void) {
    status |= STATUS_RDY_LPS331;
}

void hts221_cb(void) {
    status |= STATUS_RDY_HTS221;
}

void stlm75_cb(void) {
    status |= STATUS_RDY_STLM75;
}

void tsl2572_cb(void) {
    status |= STATUS_RDY_TSL2572;
}
/*------------------------------------ MAIN ---------------------------------------*/

int main() {
    timer_init(HSI_HZ, TIMER_BASE_FREQ);    // Start timer manager
    i2c_init();     // Init I2C communication
    LCDconfigure(); // Initialize LCD
    LCDclear();     // Clear LCD
    lcd_layout();   // Print text layout
    /*  Initialize sensors  */
    lps331ap_init(lps331ap_cb);
    hts221_init(hts221_cb);
    stlm75_init(stlm75_cb);
    tsl2572_init(tsl2572_cb);
    /*  Run system timer - call up_timer() every 1sec.  */
    timer_start(up_timer, 1000, 0);

    while (1) {
        /*  Print data from LPS331: pressure & temperature  */
        if (status & STATUS_RDY_LPS331) {
            status &= ~STATUS_RDY_LPS331;
            LCDgoto(4, 5);
            sprintf(lcd_buff, "%.1f hPa  ", lps331ap_get_pressure());
            lcd_send_string(lcd_buff);
            LCDgoto(5, 5);
            sprintf(lcd_buff, "%.1f degC  ", lps331ap_get_temperature());
            lcd_send_string(lcd_buff);
        }
        /*  Print data from HTS221: humidity & temperature  */
        if (status & STATUS_RDY_HTS221) {
            status &= ~STATUS_RDY_HTS221;
            LCDgoto(1, 5);
            sprintf(lcd_buff, "%.1f %%  ", hts221_get_rh());
            lcd_send_string(lcd_buff);
            LCDgoto(2, 5);
            sprintf(lcd_buff, "%.1f degC  ", hts221_get_temperature());
            lcd_send_string(lcd_buff);
        }
        /*  Print data from STLM75: temperature  */
        if (status & STATUS_RDY_STLM75) {
            status &= ~STATUS_RDY_STLM75;
            LCDgoto(7, 5);
            sprintf(lcd_buff, "%.1f degC  ", stlm75_get_temperature());
            lcd_send_string(lcd_buff);
        }
        /*  Print data from TSL2572: lux */
        if (status & STATUS_RDY_TSL2572) {
            status &= ~STATUS_RDY_TSL2572;
            LCDgoto(9, 5);
            sprintf(lcd_buff, "%.1f lux  ", tsl2572_get_data());
            lcd_send_string(lcd_buff);
        }
    }

}
