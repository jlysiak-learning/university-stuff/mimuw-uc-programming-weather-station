/**
 * @file
 * @ingroup i2c
 */
//--- IMPORTANT MODULES
#include <stm32.h>
#include <gpio.h>
#include <timer.h>
//--- INTERFACE
#include <i2c.h>
//--- CONFIG
#include <conf_hw.h>
#include <i2c_conf.h>

/*------------------------ I2C QUEUE STRUCTURES -----------------------------------------*/
/**
 * I2C communication request
 */
typedef struct {
    uint8_t dev;                        //!< Device hw address
    uint8_t tx_buff[I2C_TX_BUFF_SIZE];  //!< Data to send
    volatile uint8_t tx_b;              //!< Tx buffer begin
    volatile uint8_t tx_l;              //!< bytes left to send
    volatile uint8_t *rx_buff;          //!< Rx buffer
    volatile uint8_t rx_l;              //!< Bytes left to received
    void (*callback)(uint8_t err);      //!< Request complete callback
} i2c_req_t;

/**
 * I2C communication requests queue.
 * Ringbuffer structure.
 */
typedef struct {
    i2c_req_t requests[I2C_Q_SIZE]; //!< Requests
    volatile uint8_t b; //!< Begin
    volatile uint8_t l; //!< Length
} i2c_req_queue_t;
/*------------------------ QUEUE & TIMEOUT TIMER ---------------------------------------*/

static i2c_req_queue_t queue;       //!< Request queue
static volatile i2c_req_t *front;   //!< Pointer at first element in queue. (for simplicity)
static volatile i2c_req_t *back;    //!< Pointer at first free element in queue. (for simplicity)
static tim_t *timer;                //!< Timeout timer handler

/** Milliseconds left to hangup transmission. Each timer tick decrement this counter.
 * Each event sets counter to @ref I2C_TIMEOUT. When counter reaches 0, actual transmission is hanging up.
 */
static volatile uint16_t time_left;
/*----------------------------- STATUS REGISTER ----------------------------------------*/

#define I2C_REG_BUSY    0x01    //!< I2C machine busy
#define I2C_REG_MR      0x02    //!< I2C master receive mode
static volatile uint8_t status; //!< I2C status registe
/*-------------------------------IMPLEMENTATION ----------------------------------------*/
/**
 * Initialize transmission over I2C
 * if module was not busy.
 */
static void i2c_begin(void);
/**
 * Finish active request and move to
 * next one if queue is not empty.
 * If transmission is ended due to timeout timer, err RES_TIMEOUT.
 * @param err transfer status
 */
static void i2c_end(uint8_t err);

/**
 * I2C timer.
 * Decrement timeout counter. If counter reaches zero then hang up transmission.
 * Period: 1ms
 * @param r repetitions
 */
static void i2c_timer(uint16_t r);

/**
 * Refresh I2C counter by setting @ref I2C_TIMEOUT value.
 * Each bit set in SR register (should) refresh counter.
 */
static inline void i2c_timer_refresh(void) {
    time_left = I2C_TIMEOUT;
}

void i2c_init(void) {
    // Clear structures
    status = 0;
    queue.b = 0;
    queue.l = 0;
    front = queue.requests;
    back = queue.requests;

    // Setup clocks: block B & I2C1
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
    RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;
    // SCL, SDA - alternative function I2C1, low speed, open-drain, no pullup (external resistor used)
    GPIOafConfigure(I2C_SCL_GPIO, I2C_SCL_PIN, GPIO_OType_OD, GPIO_Low_Speed, GPIO_PuPd_NOPULL, GPIO_AF_I2C1);
    GPIOafConfigure(I2C_SDA_GPIO, I2C_SDA_PIN, GPIO_OType_OD, GPIO_Low_Speed, GPIO_PuPd_NOPULL, GPIO_AF_I2C1);
    // Clear control register, reset
    I2C1->CR1 = 0;
    // Set APB freq in MHz, enable EVENTs interrupts including buffers (TxE, RxNE)
    I2C1->CR2 = CLK_APB_MHZ | I2C_CR2_ITBUFEN | I2C_CR2_ITEVTEN;
    // Set timing
    I2C1->CCR = (CLK_APB_MHZ * MHZ) / (I2C_SPEED << 1);
    // Config maximum rise time
    I2C1->TRISE = CLK_APB_MHZ + 1;
    // Enable I2C EVENTS interrupt
    NVIC_EnableIRQ(I2C1_EV_IRQn);
    // Enable I2C
    I2C1->CR1 |= I2C_CR1_PE;
}

static void i2c_timer(uint16_t r) {
    time_left -= I2C_TIMER_PERIOD;
    if (!time_left) { // time's up! End transmission.
        // Free I2C bus
        I2C1->CR1 |= I2C_CR1_STOP;
        // Finish transmission with TIMEOUT error.
        i2c_end(I2C_REQ_RES_TIMEOUT);
    }
}

static void i2c_end(uint8_t err) {
    // First clear MR mode
    status &= ~I2C_REG_MR;
    // Run callback, pass result code
    (*(front->callback))(err);
    // Shift front pointer
    front = &queue.requests[(++queue.b) & I2C_Q_MOD];
    --(queue.l);
    if (queue.l) { // Queue not empty
        // Before starting new transaction, refresh timer
        i2c_timer_refresh();
        // Start transmission
        I2C1->CR1 |= I2C_CR1_START;
    }
    else {
        // I2C module is free
        status &= ~I2C_REG_BUSY;
        // Stop timer
        timer_remove(timer);
    }
}

static void i2c_begin(void) {
    // Start timeout timer
    timer = timer_start(i2c_timer, I2C_TIMER_PERIOD, 0);
    // Device is busy
    status |= I2C_REG_BUSY;
    // Start transmission
    I2C1->CR1 |= I2C_CR1_START;
}

void i2c_comm(uint8_t addr, uint8_t n_w, uint8_t *data_w, uint8_t n_r, uint8_t *data_r, void (*callback)(uint8_t)) {
    // If queue has free slots, add request.
    if (queue.l < I2C_Q_SIZE) {
        // Request structure contains:
        // callback function - signalizing end of transfer and passing result code
        back->callback = callback;
        // slave hardware address
        back->dev = addr;
        // destination of received data
        back->rx_buff = data_r;
        // how many bytes should be read
        back->rx_l = n_r;
        // reset tx buffer begin position
        back->tx_b = 0;
        // Copy data to send
        while (n_w--)
            back->tx_buff[(back->tx_l)++] = *(data_w++);
        (queue.l)++;
        // Move back pointer to new free slot
        back = &queue.requests[(queue.b + queue.l) & I2C_Q_MOD];
        if (!(status & I2C_REG_BUSY)) // Begin transfer if I2C was off
            i2c_begin();
    } else { // Queue is full
        // Send full queue result code
        (*callback)(I2C_REQ_RES_QUEUE_FULL);
    }
}

/**
 * I2C event interrupt handler.
 */
void I2C1_EV_IRQHandler(void) {
    uint32_t state = I2C1->SR1;
    // EV5 - Start-bit transmission finished
    if (state & I2C_SR1_SB) {
        // Write hardware address
        if (status & I2C_REG_MR) {  // If MR mode is ON, set LSB in hw address
            I2C1->DR = (front->dev << 1) | 1U;
            if (front->rx_l == 1)       // Set NACK bit BEFORE reading LAST (AND ONLY) byte!
                I2C1->CR1 &= ~I2C_CR1_ACK;
            else                        // Otherwise send ACK
                I2C1->CR1 |= I2C_CR1_ACK;
        } else {    // Otherwise (TM mode), just shift hw address left
            I2C1->DR = front->dev << 1;
        }
        i2c_timer_refresh(); // Transmission works, reset timeout timer
    }
    // EV6 - Address transmission finished
    if (state & I2C_SR1_ADDR) {
        I2C1->SR2;                      // Reset flag (required, just read SR2, check STM32 doc)
        i2c_timer_refresh();            // Transmission still works, reset timeout timer
        if (status & I2C_REG_MR) {      // I2C in Master Receving mode
            if (front->rx_l == 1)       // Request sending STOP signal after LAST (AND ONLY) byte
                I2C1->CR1 |= I2C_CR1_STOP;
        } else {    // Master transmitting mode, obviously...
            // Now it's time to send slave device memory address
            // First byte to write is ALWAYS memory address (subaddress)
            I2C1->DR = front->tx_buff[(front->tx_b)++];
            (front->tx_l)--;
        }
    }
    // TxE is set after byte transmission. It's signal to load next byte into DR.
    if ((state & I2C_SR1_TXE) && front->tx_l) {
        i2c_timer_refresh();    // Transmission still works, reset timeout timer
        I2C1->DR = front->tx_buff[(front->tx_b)++];   // Load data to DataRegister
        (front->tx_l)--;
    }
    // TxE and BTF flag is set. Transmission completed.
    if ((state & I2C_SR1_BTF) && !(front->tx_l)) {
        i2c_timer_refresh();    // Transmission works, reset timeout timer
        if (front->rx_l) {  // Data receiving requested?
            I2C1->CR1 |= I2C_CR1_START; // Request REPEATED START!
            status |= I2C_REG_MR;       // Set Master Receive mode
        } else {        // No data to receive -> STOP signal
            I2C1->CR1 |= I2C_CR1_STOP;
            i2c_end(I2C_REQ_RES_OK);  // Finish active i2c request, result: success
        }
    }
    // Receiver Data Register Not Empty, byte was received...
    if (state & I2C_SR1_RXNE) {
        i2c_timer_refresh();    // Transmission works, reset timeout timer
        *(front->rx_buff) = I2C1->DR; // Read byte
        front->rx_buff++;
        (front->rx_l)--;
        if (front->rx_l == 1) {     // Only one left? So, clear ACK and request STOP signal
            I2C1->CR1 &= ~I2C_CR1_ACK;
            I2C1->CR1 |= I2C_CR1_STOP;
        } else if (!front->rx_l) { // If it's all..
            i2c_end(I2C_REQ_RES_OK); // ..., finish active i2c request
        }
    }
}