/**
 * @file
 * @ingroup i2c
 * I2C configuration.
 */
#ifndef I2C_CONF_H
#define I2C_CONF_H

#define I2C_SPEED           100000U //!< I2C clock frequency

#define I2C_SDA_GPIO        GPIOB   //!< SDA line GPIO
#define I2C_SCL_GPIO        GPIOB   //!< SCL line GPIO
#define I2C_SDA_PIN         9       //!< SDA line pin
#define I2C_SCL_PIN         8       //!< SCL line pin

#define I2C_TIMEOUT         200     //!< I2C timeout limit [ms]
#define I2C_TIMER_PERIOD    1       //!< I2C timer period [ms]
#define I2C_TX_BUFF_SIZE    32      //!< TX buffer size
#define I2C_Q_SIZE          32      //!< I2C request queue size. Must be power of 2 for fast modulo purpose.
#define I2C_Q_MOD           31      //!< I2C queue size modulo constant

#endif // I2C_CONF_H
