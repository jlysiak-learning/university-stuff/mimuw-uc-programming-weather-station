/**
 * @defgroup i2c
 * @author Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 * @date 10.12.2016
 * I2C interface.
 * Only MASTER RX/TX implemented.
 */

/**
 * @file
 * @ingroup i2c
 */

#ifndef I2C_H_H
#define I2C_H_H

#include <stdint.h>

/**
 * @ingroup i2c
 * @addtogroup i2c_comm_res_codes
 * I2C communication request result codes.
 * @{
 */
#define I2C_REQ_RES_OK          0
#define I2C_REQ_RES_TIMEOUT     1
#define I2C_REQ_RES_QUEUE_FULL  2
/** @} */

/**
 * I2C initialization.
 */
void i2c_init(void);

/**
 * Communicate over I2C with device having 'hw_addr'.
 * Write n_w bytes stored in 'data_w'.
 * Read n_r bytes to 'data_r' buffer.
 * @param addr device hardware addres
 * @param n_w bytes to write
 * @param data_w data to write
 * @param n_r bytes to read
 * @param data_r data buffer
 * @param callback Transfer complete callback
 */
void i2c_comm(uint8_t addr, uint8_t n_w, uint8_t *data_w,
              uint8_t n_r, uint8_t *data_r, void (*callback)(uint8_t));

#endif /* I2C_H_H */
