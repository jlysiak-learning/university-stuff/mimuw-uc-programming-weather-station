/**
 * @file
 * @ingroup stlm75
 * @author Jacek Łysiak <jaceklysiako.o@gmail.com>
 * STLM75 configuration file
 */
#ifndef STLM75_CONF_H
#define STLM75_CONF_H

#define STLM75_HW_ADDR              0x48    //!< Device hardware address
/*--------------------------  REGISTERS ADDRESSES & REGISTERS' BITS  ----------------*/

#define STLM75_TEMP_ADDR            0x00    //!< Temperature register, 16b
#define STLM75_CONF_ADDR            0x01    //!< Configuration register 3 address, uint8_t

#define STLM75_CONF_SD              0x01    //!< Shutdown bit @warning SD bit = 1 means SHUTDOWN!
#define STLM75_CONF_TM              0x02    //!< Thermostat mode
#define STLM75_CONF_POL             0x04    //!< Output polarity
#define STLM75_CONF_FT0             0x08    //!< Fault tolerance 0bit
#define STLM75_CONF_FT1             0x10    //!< Fault tolerance 1bit

#endif /* STLM75_CONF_H */
