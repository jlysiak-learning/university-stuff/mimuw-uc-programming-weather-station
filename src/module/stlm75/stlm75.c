/**
 * @file
 * @ingroup stlm75
 */
//--- MODULES
#include <i2c.h>
//--- INTERFACE
#include <stlm75.h>
//--- CONFIG
#include <stlm75_conf.h>

#ifndef STLM75_HW_ADDR  // check hardware address
#error Using this module requires specifying chip hardware address.
#endif
/*----------------------------- DEVICE CTRL -------------------------------------*/
/**
 * STLM75 registers
 */
typedef struct {
    uint16_t temp;  //!< Temperature register, 0x00
    uint8_t conf;   //!< Configuration register, 0x01
} stlm75_reg_t;

/// Device states
enum dev_state_t {
    DS_CONFIG,      //!< Initialization
    DS_READY,       //!< Device is ready
    DS_READ_DATA    //!< Data reading in progress
};

static volatile uint8_t dev_state;  //!< Device state indicator
static stlm75_reg_t registers;      //!< Device registers
static void (*data_rdy_call)(void); //!< Data ready callback
static float temperature;           //!< Actual temperature
/*----------------------------- I2C COMMUNICATION ------------------------------*/

#define I2C_BUFF_SIZE   2   //!<  I2C communication buffer size
static uint8_t i2c_buff[I2C_BUFF_SIZE]; //!< Data buffer for i2c communication

/**
 * I2C data reception callback.
 */
static void stlm75_i2c_callback(uint8_t err) {
    switch (dev_state) {
        case DS_CONFIG:
            dev_state++; // -> DS_READY, Configuration done, device is working now...
            break;

        case DS_READ_DATA:
            // Copy register
            registers.temp = (uint16_t)(i2c_buff[1] | ((uint16_t) i2c_buff[0] << 8));
            // Calculate actual temperature
            // MS byte is 2's complement integer part
            temperature = (int8_t) i2c_buff[0];
            // MS bit in LS byte is fractional part
            if (i2c_buff[1]) {
                if (i2c_buff[0] & 0x80)
                    temperature -= 0.5;
                else
                    temperature += 0.5;
            }
            dev_state = DS_READY;
            // Signalize data reception
            (*data_rdy_call)();
            break;
    }
}

/*------------------------------- INTERFACE ------------------------------------*/

void stlm75_init(void (*callback)(void)) {
    data_rdy_call = callback;
    // Set config state
    dev_state = DS_CONFIG;
    i2c_buff[0] = STLM75_CONF_ADDR;
    // Clear register, shutdown bit = 1 means power off device
    registers.conf = 0;
    i2c_buff[1] = registers.conf;
    //  Queue request: set configuration register
    i2c_comm(STLM75_HW_ADDR, 2, i2c_buff, 0, 0, stlm75_i2c_callback);
}

void stlm75_measure(void) {
    if (dev_state == DS_READY) {
        dev_state = DS_READ_DATA;
        // Read measurement data
        i2c_buff[0] = STLM75_TEMP_ADDR;
        // Queue request: read two bytes of data
        i2c_comm(STLM75_HW_ADDR, 1, i2c_buff, 2, i2c_buff, stlm75_i2c_callback);
    }
}

float stlm75_get_temperature(void) {
    return temperature;
}