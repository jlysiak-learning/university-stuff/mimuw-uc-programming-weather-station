/**
 * @defgroup stlm75 STLM75
 * @author Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 * STLM75 temperature sensor.
 * Hardware configuration:
 *  - STM32f411xx Nucleo board
 *  - KA-Nucleo-Weather shield
 */

/**
 * @file
 * @ingroup stlm75
 */

#ifndef STLM75_H
#define STLM75_H

/**
 * Initialize device.
 * Require @ref i2c module initialization.
 * @param callback data ready handler
 */
void stlm75_init(void (*callback)(void));

/**
 * Start measurement.
 * When data is ready, callback is calling.
 * According to datasheet, measurement lasts ~150ms. Don't call this
 * function with smaller interval. Result is buffered and will not be updated
 * when I2C transmission is ongoing.
 */
void stlm75_measure(void);

/**
 * Get data.
 * @return chip temperature in C deg.
 */
float stlm75_get_temperature(void);

#endif /* STLM75_H */