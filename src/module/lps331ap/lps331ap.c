/**
 * @file
 * @ingroup lps331ap
 * LPS331AP device handling implementation
 */
//--- MODULES
#include <gpio.h>
#include <i2c.h>
#include <timer.h>
//--- INTERFACE
#include <lps331ap.h>
//--- CONFIG
#include <lps331ap_conf.h>

#ifndef LPS331AP_HW_ADDR     // check hardware address
#error Using this module requires specifying chip hardware address.
#endif
/*-------------------------------  REGISTERS  & DEV STATES  ------------------------------------------*/

/**
 * LPS331AP registers.
 * Registers description: see registers' addresses
 */
typedef struct {
    uint8_t stat_reg;
    uint32_t press_out;
    int16_t temp_out;
} lps331ap_reg_t;

/**
 * Device states
 */
enum dev_state_t {
    DS_OFF,         //!< Device is off
    DS_CONF_RES,    //!< Set measurement resolution
    DS_STARTUP,     //!< Power up device
    DS_READY,       //!< Device is ready
    DS_REQ,         //!< Send measurement request
    DS_POOLING,     //!< Check status register
    DS_READING      //!< Get data
};
/*-------------------------------------- DEVICE CTRL -------------------------------------------------*/

static volatile uint8_t dev_state;      //!< Device state indicator
static lps331ap_reg_t registers;        //!< Device registers
static void (*data_rdy_call)(void);     //!< Data ready callback

static float pressure;                  //!< Measured pressure
static float temperature;               //!< Measured temperature
static tim_t *timer;                    //!< Pooling timer handler
/*------------------------------------- I2C COMMUNICATION ---------------------------------------------*/

#define I2C_BUFF_SIZE   16              //!< I2C communication buffer size
static uint8_t i2c_buff[I2C_BUFF_SIZE]; //!< Data buffer for i2c communication

/**
 *  Check status register.
 *  Calling by timer every 0.1s.
 *  @param r reps
 */
static void i2c_pooling(uint16_t r);

/**
 * I2C data reception callback.
 */
static void lps331ap_i2c_callback(uint8_t err) {
    switch (dev_state) {
        case DS_OFF:
            dev_state++; // -> DS_CONF_RES
            break;

        case DS_CONF_RES:
            dev_state++; // -> DS_STARTUP
            break;

        case DS_STARTUP: // -> DS_READY
            dev_state++;
            break;

        case DS_REQ:
            dev_state = DS_POOLING;
            // Start pooling
            timer = timer_start(i2c_pooling, 100, 0);
            break;

        case DS_POOLING:
            registers.stat_reg = i2c_buff[0];
            if ((registers.stat_reg & LPS331AP_STAT_DRDY) == LPS331AP_STAT_DRDY) {
                dev_state = DS_READING;
                // Read data, 5 bytes - pressure + temperature
                i2c_buff[0] = LPS331AP_MULTIBYTE | LPS331AP_PRESS_POUT_XL_REH_ADDR;
                i2c_comm(LPS331AP_HW_ADDR, 1, i2c_buff, 5, i2c_buff, lps331ap_i2c_callback);
                // Remove pooling timer
                timer_remove(timer);
            }
            break;

        case DS_READING:
            dev_state = DS_READY;
            // Copy results
            registers.press_out = i2c_buff[0] | ((uint32_t) i2c_buff[1] << 8) | ((uint32_t) i2c_buff[2] << 16);
            registers.temp_out = (int16_t) (i2c_buff[3] | ((uint16_t) i2c_buff[4] << 8));
            // Caluculate pressure and temperature (see datasheet)
            pressure = registers.press_out / 4096.0;
            temperature = 42.5 + registers.temp_out / (4.0 * 120);
            // signalize data ready
            (*data_rdy_call)();
            break;
    }
}

static void i2c_pooling(uint16_t r) {
    // Read status register
    i2c_buff[0] = LPS331AP_STATUS_ADDR | LPS331AP_MULTIBYTE;
    i2c_comm(LPS331AP_HW_ADDR, 1, i2c_buff, 1, i2c_buff, lps331ap_i2c_callback);
}
/*------------------------------------- INTERFACE ---------------------------------------------*/
void lps331ap_init(void (*callback)(void)) {
    data_rdy_call = callback;
    dev_state = DS_OFF;

    // Set CTRL1 register address
    i2c_buff[0] = LPS331AP_CTRL1_ADDR;
    // Clear register, power off device, clean start
    i2c_buff[1] = 0;
    // Queue I2C request
    i2c_comm(LPS331AP_HW_ADDR, 2, i2c_buff, 0, 0, lps331ap_i2c_callback);

    // Set best resolution
    i2c_buff[0] = LPS331AP_RES_CONF_ADDR;
    i2c_buff[1] = LPS331AP_RES_CONF_MAX;
    i2c_comm(LPS331AP_HW_ADDR, 2, i2c_buff, 0, 0, lps331ap_i2c_callback);

    // Power up device
    i2c_buff[0] = LPS331AP_CTRL1_ADDR;
    // One shot measurement configuration
    i2c_buff[1] = LPS331AP_CTRL1_ODR_OS | LPS331AP_CTRL1_PD;
    i2c_comm(LPS331AP_HW_ADDR, 2, i2c_buff, 0, 0, lps331ap_i2c_callback);
}

float lps331ap_get_pressure(void) {
    return pressure;
}

float lps331ap_get_temperature(void) {
    return temperature;
}

void lps331ap_measure(void) {
    if (dev_state == DS_READY) {
        dev_state = DS_REQ;
        // Request measurement
        i2c_buff[0] = LPS331AP_CTRL2_ADDR;
        // One shot enable
        i2c_buff[1] = LPS331AP_CTRL2_OS_EN;
        i2c_comm(LPS331AP_HW_ADDR, 2, i2c_buff, 0, 0, lps331ap_i2c_callback);
    }
}