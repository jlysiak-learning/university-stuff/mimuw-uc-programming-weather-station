/**
 * @file
 * @ingroup lps331ap
 * LPS331AP configuration file.
 */

#ifndef LPS331AP_CONF_H
#define LPS331AP_CONF_H

#define LPS331AP_HW_ADDR                0x5D    //!< LPS331AP hardware address
/*--------------------------  REGISTERS ADDRESSES & REGISTERS' BITS  ----------------------------------*/

#define LPS331AP_REF_P_XL_ADDR          0x08    //!< Reference pressure, low byte
#define LPS331AP_REF_P_L_ADDR           0x09    //!< Reference pressure, middle byte
#define LPS331AP_REF_P_H_ADDR           0x0A    //!< Reference pressure, high byte
#define LPS331AP_WHOAMI_ADDR            0x0F    //!< Device identification
#define LPS331AP_RES_CONF_ADDR          0x10    //!< Pressure & temperature resolution mode
#define LPS331AP_CTRL1_ADDR             0x20    //!< Control reg. 1
#define LPS331AP_CTRL2_ADDR             0x21    //!< Control reg. 2
#define LPS331AP_CTRL3_ADDR             0x22    //!< Interrupt control
#define LPS331AP_INT_CFG_ADDR           0x23    //!< Interrupt configuration
#define LPS331AP_INT_SRC_ADDR           0x24    //!< Interrupt source
#define LPS331AP_THS_P_L_ADDR           0x25    //!< Threshold pressure, low byte
#define LPS331AP_THS_P_H_ADDR           0x26    //!< Threshold pressure, high byte
#define LPS331AP_STATUS_ADDR            0x27    //!< Status register
#define LPS331AP_PRESS_POUT_XL_REH_ADDR 0x28    //!< Pressure data, low byte
#define LPS331AP_PRESS_OUT_L_ADDR       0x29    //!< Pressure data, mid byte
#define LPS331AP_PRESS_OUT_H_ADDR       0x2A    //!< Pressure data, high byte
#define LPS331AP_TEMP_OUT_L_ADDR        0x2B    //!< Temperature data, low byte
#define LPS331AP_TEMP_OUT_H_ADDR        0x2C    //!< Temperature data, high byte
#define LPS331AP_AMP_CTRL_ADDR          0x30    //!<  Analog front end control (datasheet ambiguity...? Different addresses..)

#define LPS331AP_MULTIBYTE              0x80    //!< Should be always set in each subaddress

#define LPS331AP_CTRL1_PD               0x80    //!< Power up device
#define LPS331AP_CTRL1_BDU              0x04    //!< Block data update
#define LPS331AP_CTRL1_ODR_1HZ          0x10    //!< Output data rate = 1Hz
#define LPS331AP_CTRL1_ODR_OS           0x00    //!< Output data rate = one shot

#define LPS331AP_CTRL2_OS_EN            0x01    //!< Run measurement

#define LPS331AP_CTRL3_INT_L            0x80    //!< Active low interrupt signal
#define LPS331AP_CTRL3_INT_OD           0x40    //!< Open drain interrupt output
#define LPS331AP_CTRL3_INT1_DRDY        0x04    //!< INT1 -> data ready interrupt

#define LPS331AP_STAT_DRDY              0x03    //!< All data available

#define LPS331AP_RES_CONF_MAX           0x7A    //!< Maximal resolution, 128 avg. temp, 512 avg. pressure, RMS(p) noise = 0.02 mbar
#define LPS331AP_RES_CONF_256P_64T      0x68    //!< 64 avg. temp, 256avg. pressure, RMS(p) noise = 0.03 mbar

#endif /* LPS331AP_CONF_H */
