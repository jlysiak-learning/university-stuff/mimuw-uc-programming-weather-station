/**
 * @defgroup lps331ap LPS331AP
 * @author Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 * LPS331AP pressure & temperature sensor.
 * Hardware configuration:
 *  - STM32f411xx Nucleo board
 *  - KA-Nucleo-Weather shield
 */

/**
 * @file
 * @ingroup lps331ap
 */

#ifndef LPS331AP_H
#define LPS331AP_H

/**
 * Initialize device.
 * Require @ref i2c module initialization.
 * Function configures proper peripheral blocks, initializes
 * device structures and queues i2c config read/write requests.
 * @param callback data ready handler
 */
void lps331ap_init(void (*callback)(void));

/**
 * Start measurement.
 */
void lps331ap_measure(void);

/**
 * Get pressure.
 * @return pressure [mBar]
 */
float lps331ap_get_pressure(void);

/**
 * Get temperature.
 * @return temperature [degC]
 */
float lps331ap_get_temperature(void);

#endif /* LPS331AP_H */