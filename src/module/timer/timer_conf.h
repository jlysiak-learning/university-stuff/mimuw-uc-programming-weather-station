/**
 * @file
 * @ingroup timers
 */
#ifndef TIMER_CONF_H
#define TIMER_CONF_H

#include <stm32.h>

/// Maximum number of registered timers
#define TIMER_MAX               32

#endif /* TIMER_CONF_H */
