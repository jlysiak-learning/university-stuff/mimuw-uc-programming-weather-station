/**
 * @file
 * @ingroup timers
 */
//--- IMPORTANT MODULES
#include <stm32.h>
//--- INTERFACE
#include <timer.h>
//--- CONFIG
#include <timer_conf.h>

/// Check whether timer t is used
#define timer_in_use(t)     ((t)->flags & TIMER_FLG_USED)
/// Set flag f in timer t register
#define timer_set(t, f)     ((t)->flags |= (f))
/// Clear flag f in timer t register
#define timer_clr(t, f)     ((t)->flags &= ~(f))

#define TIMER_FLG_USED      0x01    //!< Timer slot is actually in use

/** Software timer handler */
typedef struct tim_t {
    uint8_t flags;          //!< Timer flags
    uint16_t cnt;           //!< Active period counter
    uint16_t len;           //!< Period duration
    uint16_t cnt_r;         //!< Active reps counter
    uint16_t tot_r;         //!< Total reps
    void (*h)(uint16_t);    //!< Timer event handler passing @ref cnt_r
} tim_t;

static tim_t timers[TIMER_MAX]; //!< Timer handlers
static uint8_t timers_s;        //!< Total number of registered timers
static uint8_t iter;            //!< Timers iterator
static tim_t *iter_p;           //!< Timer pointer iterator

/*----------------------------------  IMPLEMENTATION  ------------------------------------*/
/**
 * Initialize hardware stuff.
 * @param fclk
 * @param ftim
 */
static void timer_conf_hw(uint32_t fclk, uint32_t ftim) {
    RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
    TIM1->PSC = 9;
    // set timing to reach ftim frequency
    TIM1->ARR = fclk / ftim / 10 - 1;
    // Enable event interrupt
    TIM1->DIER = TIM_DIER_UIE;
    // Enable timer
    TIM1->CR1 = TIM_CR1_CEN;
    // Enable interrupt handler
    NVIC_EnableIRQ(TIM1_UP_TIM10_IRQn);
}

void timer_init(uint32_t fclk, uint32_t ftim) {
    timers_s = 0;
    for (iter = 0; iter < TIMER_MAX; ++iter)
        timers[iter].flags = 0;
    timer_conf_hw(fclk, ftim);
}

tim_t *timer_start(void (*h)(uint16_t), uint16_t period, uint16_t reps) {
    // Find free slot for timer handler
    for (iter = 0, iter_p = timers; iter < TIMER_MAX; ++iter, ++iter_p) {
        if (!timer_in_use(iter_p)) { // Slot not used, take it
            timer_set(iter_p, TIMER_FLG_USED);
            iter_p->cnt = 0;
            iter_p->len = period;
            iter_p->cnt_r = 0;
            iter_p->tot_r = reps;
            iter_p->h = h;
            ++timers_s;
            return iter_p; // return timer handler
        }
    }
    return 0; // no free slot
}

void timer_remove(tim_t *t) {
    if (t) {
        timer_clr(t, TIMER_FLG_USED); // free timer
        --timers_s;
    }
}

uint32_t timer_check(tim_t *t) {
    // Total time = reps * period + actual counter
    return t->cnt_r * t->len + t->cnt;
}

/**
 * Hardware timer tick handler.
 * @ingroup isr
 */
void timer_irq_handler(void) {
    if (TIM1->SR & TIM_SR_UIF) {
        TIM1->SR = ~TIM_SR_UIF;
        for (iter = 0, iter_p = timers; iter < TIMER_MAX; ++iter, ++iter_p) {
            if (timer_in_use(iter_p)) {
                iter_p->cnt++; // Increase period counter
                if (iter_p->cnt == iter_p->len) { // end of period
                    iter_p->cnt = 0; // Start counting again
                    iter_p->cnt_r++; // Increase reps
                    if (iter_p->h) // check null pointer
                        (*iter_p->h)(iter_p->cnt_r); // call handler with actual rep as arg.
                    if (iter_p->cnt_r == iter_p->tot_r)
                        if (iter_p->tot_r != 0)
                            timer_remove(iter_p); // remove handler
                }
            }
        }
    }
}
