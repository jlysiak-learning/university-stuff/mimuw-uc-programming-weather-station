/**
 * @file
 * @ingroup tsl2572
 * TSL2572 configuration file.
 */

#ifndef TSL2572_CONF_H
#define TSL2572_CONF_H

#define TSL2572_HW_ADDR         0x39    //!< TSL2572 hardware address

#define TSL2572_EXTI_GPIO       GPIOA   //!< TSL2572 external interrupt port
#define TSL2572_EXTI_PIN        0       //!< TSL2572 external interrupt pin

#define CONF_ATIME              699.0   //!< ATIME selected
#define CONF_AGAIN              1.0     //!< AGAIN selected
#define CONF_GA                 1.0     //!< Glass attenuation
/*--------------------------  REGISTERS ADDRESSES & REGISTERS' BITS  ----------------------------------*/

#define TSL2572_ENABLE_ADDR     0x00    //!< Enables  states  and  interrupts
#define TSL2572_ATIME_ADDR      0x01    //!< ALS integration time
#define TSL2572_WTIME_ADDR      0x03    //!< Wait time
#define TSL2572_AILTL_ADDR      0x04    //!< ALS interrupt low threshold low byte
#define TSL2572_AILTH_ADDR      0x05    //!< ALS interrupt low threshold high byte
#define TSL2572_AIHTL_ADDR      0x06    //!< ALS interrupt high threshold low byte
#define TSL2572_AIHTH_ADDR      0x07    //!< ALS interrupt high threshold high byte
#define TSL2572_PERS_ADDR       0x0C    //!< Interrupt persistence filters
#define TSL2572_CONFIG_ADDR     0x0D    //!< Configuration
#define TSL2572_CONTROL_ADDR    0x0F    //!< Control register
#define TSL2572_ID_ADDR         0x12    //!< Device ID
#define TSL2572_STATUS_ADDR     0x13    //!< Status register
#define TSL2572_C0DATA_ADDR     0x14    //!< CH0 ADC low data register
#define TSL2572_C0DATAH_ADDR    0x15    //!< CH0 ADC high data register
#define TSL2572_C1DATA_ADDR     0x16    //!< CH1 ADC low data register
#define TSL2572_C1DATAH_ADDR    0x17    //!< CH1 ADC high data register

#define TSL2572_CMD_REP         0x80    //!< Repeated byte protocol transaction command
#define TSL2572_CMD_AUTOINC     0xA0    //!< Auto-increment protocol transaction command
#define TSL2572_CMD_CLRALS      0xE6    //!< ALS interrupt clear special function

#define TSL2572_ENABLE_PON      0x01    //!< Power ON
#define TSL2572_ENABLE_AEN      0x02    //!< ALS enable
#define TSL2572_ENABLE_WEN      0x08    //!< Wait time enable
#define TSL2572_ENABLE_AIEN     0x10    //!< ALS interrupt enable
#define TSL2572_ENABLE_SAI      0x20    //!< Sleep after interrupt enable

#define TSL2572_ATIME_1         0xFF    //!< 1 cycle, 2.73ms
#define TSL2572_ATIME_10        0xF6    //!< 10 cycles, 27.3ms
#define TSL2572_ATIME_37        0xDB    //!< 37 cycles, 101ms
#define TSL2572_ATIME_64        0xC0    //!< 64 cycles, 175ms
#define TSL2572_ATIME_256       0x00    //!< 256 cycles, 699ms

#define TSL2572_WTIME_1         0xFF    //!< 1 time, 2.73ms
#define TSL2572_WTIME_74        0xB6    //!< 74 times, 202ms
#define TSL2572_WTIME_256       0x00    //!< 256 times, 699ms

#endif /* TSL2572_CONF_H */
