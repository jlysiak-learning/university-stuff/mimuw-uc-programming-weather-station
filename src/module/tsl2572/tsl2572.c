/**
 * @file
 * @ingroup tsl2572
 * @note If you want to use interrupts it's important to power off device first
 * in case of uC reset which doesn't reset sensor. EXTI can occur before device configuration.
 * IC's supply cannot be controlled by uC, so it must be done by conf. registers.
 */
//--- MODULES
#include <i2c.h>
#include <gpio.h>
//--- INTERFACE
#include <tsl2572.h>
//--- CONFIG
#include <tsl2572_conf.h>

#ifndef TSL2572_HW_ADDR     // check hardware address
#error Using this module requires specifying chip hardware address.
#endif
/*-------------------------------  REGISTERS  & DEV STATES  ------------------------------------------*/

/**
 * TSL2572 registers.
 * Registers description: see registers' addresses
 */
typedef struct {
    uint8_t enable;
    uint8_t atime;
    uint8_t wtime;
    uint8_t ailtl;
    uint8_t ailth;
    uint8_t aihtl;
    uint8_t aihth;
    uint8_t pers;
    uint8_t config;
    uint8_t control;
    uint8_t id;
    uint8_t status;
    uint16_t c0data;
    uint16_t c1data;
} tsl2572_reg_t;

/// Device states
enum dev_state_t {
    DS_OFF,         //!< Power off
    DS_PRE_CLR,     //!< Clear interrupt flag
    DS_C_ATIME,     //!< Set integration time
    DS_PWR_ON,      //!< Power up device
    DS_READY,       //!< Ready
    DS_CLR_INT,     //!< Clear interrupt
    DS_READ_DATA    //!< Get data
};
/*-------------------------------  RESULTS  ----------------------------------------------*/

static float cpl;       //!< Counts per lux
static float lux1;      //!< Lux 1: fluorescent and incandescent light.
static float lux2;      //!< Lux 2: dimmed incandescent light
static float lux;       //!<Result lux
/*------------------------------------------------------------------------------------------------*/

static volatile uint8_t dev_state;          //!< Device state indicator
static tsl2572_reg_t registers;             //!< Device registers
static void (*data_rdy_call)(void);         //!< Data ready callback

#define I2C_BUFF_SIZE   16                  //!< I2C communication buffer size
static uint8_t i2c_buff[I2C_BUFF_SIZE];     //!< Data buffer for i2c communication

/**
 * I2C communication with device.
 */
static void tsl2572_i2c_callback(uint8_t err) {
    switch (dev_state) {
        case DS_OFF: // Device off
            dev_state++;
            break;

        case DS_PRE_CLR: // Interrupt cleared
            dev_state++;
            break;

        case DS_C_ATIME: // Integration time set
            dev_state++; // Wait for power on
            break;

        case DS_PWR_ON:
            // Measurements are running
            // Clear STM interrupt flag
            EXTI->PR |= EXTI_PR_PR0;
            // Turn on EXT Interrupts
            NVIC_EnableIRQ(EXTI0_IRQn);
            dev_state++; // Device is ready
            break;

        case DS_CLR_INT:
            // Interrupt cleared
            // Wait for data reception
            dev_state++;
            break;

        case DS_READ_DATA:
            // New data received
            // Copy buffer
            registers.c0data = i2c_buff[0] | ((uint16_t) i2c_buff[1] << 8);
            registers.c1data = i2c_buff[2] | ((uint16_t) i2c_buff[3] << 8);
            // Calculate luminosity (see TSL2572 datasheet p. 8)
            lux1 = (1.0 * registers.c0data - 1.87 * registers.c1data) / cpl;
            lux2 = (0.63 * registers.c0data - 1.0 * registers.c1data) / cpl;
            lux = (lux1 > lux2 ? lux1 : lux2);
            lux = (lux > 0 ? lux : 0);
            // Signalize new data
            (*data_rdy_call)();
            dev_state = DS_READY;
            break;
    }
}

void tsl2572_init(void (*callback)(void)) {
    // calculate counts per lux
    cpl = CONF_AGAIN * CONF_ATIME / CONF_GA / 60.0;
    data_rdy_call = callback;
    dev_state = DS_OFF;

    // configure interrupt line
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
    RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
    // GPIO as EXIT interrupt input with pull-up
    // It's not required due to on-board pullup resistors
    GPIOinConfigure(TSL2572_EXTI_GPIO, TSL2572_EXTI_PIN, GPIO_PuPd_UP, EXTI_Mode_Interrupt, EXTI_Trigger_Falling);

    // Power off device. Set ENABLE reg address
    i2c_buff[0] = TSL2572_CMD_REP | TSL2572_ENABLE_ADDR;
    i2c_buff[1] = 0;
    // Queue I2C request
    i2c_comm(TSL2572_HW_ADDR, 2, i2c_buff, 0, 0, tsl2572_i2c_callback);

    // Perform clearing interrupt
    i2c_buff[0] = TSL2572_CMD_CLRALS; // Clear interrupt flag
    i2c_comm(TSL2572_HW_ADDR, 1, i2c_buff, 0, 0, tsl2572_i2c_callback);

    // Set ATIME reg address
    i2c_buff[0] = TSL2572_CMD_REP | TSL2572_ATIME_ADDR;
    // Integration time: 37 wait cycles = 101ms
    i2c_buff[1] = TSL2572_ATIME_256;
    registers.atime = i2c_buff[1];
    i2c_comm(TSL2572_HW_ADDR, 2, i2c_buff, 0, 0, tsl2572_i2c_callback);

    // Set ENABLE register
    i2c_buff[0] = TSL2572_CMD_REP | TSL2572_ENABLE_ADDR;
    // Power on, ALS conversion enable, sleep enable, conversion end interrupt enable
    i2c_buff[1] = TSL2572_ENABLE_AEN | TSL2572_ENABLE_AIEN | TSL2572_ENABLE_PON | TSL2572_ENABLE_SAI;
    registers.enable = i2c_buff[1];
    i2c_comm(TSL2572_HW_ADDR, 2, i2c_buff, 0, 0, tsl2572_i2c_callback);
}

float tsl2572_get_data(void) {
    return lux;
}

/**
 * ALS interrupt handler on PA0.
 */
void EXTI0_IRQHandler(void) {
    if (EXTI->PR & EXTI_PR_PR0) {
        EXTI->PR |= EXTI_PR_PR0;
        // Clear interrupt - send special command clearing interrupt flag
        dev_state = DS_CLR_INT;
        // Clear interrupt flag command
        i2c_buff[0] = TSL2572_CMD_CLRALS;
        i2c_comm(TSL2572_HW_ADDR, 1, i2c_buff, 0, 0, tsl2572_i2c_callback);
        // Read ALS result
        i2c_buff[0] = TSL2572_CMD_AUTOINC | TSL2572_C0DATA_ADDR;
        i2c_comm(TSL2572_HW_ADDR, 1, i2c_buff, 4, i2c_buff, tsl2572_i2c_callback);
    }
}