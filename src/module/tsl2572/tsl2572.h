/**
 * @defgroup tsl2572 TSL2572
 * @author Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 * TSL2572 light sensor.
 * Data ready interrupt enabled.
 * Hardware configuration:
 *  - STM32f411xx Nucleo board
 *  - KA-Nucleo-Weather shield
 */
/**
 * @file
 * @ingroup tsl2572
 */

#ifndef TSL2572_H
#define TSL2572_H

/**
 * Initialize device.
 * Configure cyclic measurements.
 * Require @ref i2c module initialization.
 * @param callback data ready handler
 */
void tsl2572_init(void (*callback)(void));

/**
 * Get data.
 * @return light intensity in lux
 */
float tsl2572_get_data(void);

#endif /* TSL2572_H */