/**
 * @file
 * @ingroup hts221
 * @TODO INTERRUPT DOESN'T WORK!??
 *       Polling working...
 */
//--- IMPORTANT MODULES
#include <i2c.h>
#include <timer.h>
//--- INTERFACE
#include <hts221.h>
//--- CONFIG
#include <hts221_conf.h>

#ifndef HTS221_HW_ADDR  // check hardware address
#error Using this module requires specifying chip hardware address.
#endif

/**
 * HTS221 registers
 */
typedef struct {
    uint8_t ctrl_reg1;      //!< Control register CTRL_REG1, 0x20
    uint8_t ctrl_reg3;      //!< Control register CTRL_REG3, 0x22
    uint8_t stat_reg;       //!< Status register, 0x27
    int16_t h_out;          //!< Humidity measurement result, 0x28
    int16_t t_out;          //!< Temperature measurement result, 0x2A
    uint8_t h0_rh_x2;       //!< H0 rH calibration register, 0x30
    uint8_t h1_rh_x2;       //!< H1 rH calibration register, 0x31
    uint16_t t0_degC_x8;    //!< T0 degC calib. register, 0x32 & 0x35
    uint16_t t1_degC_x8;    //!< T1 degC calib. register, 0x33 & 0x35
    uint8_t t1_t0_msb;      //!< T1 & T0 MS bits
    int16_t h0_t0_out;      //!< H0 T0 calibration register, 0x36
    int16_t h1_t0_out;      //!< H1 T0 calibration register, 0x3A
    int16_t t0_out;         //!< T0 calibration register, 0x3C
    int16_t t1_out;         //!< T1 calibration register, 0x3E
} hts221_reg_t;

/**
 * Device states
 */
enum dev_state_t {
    DS_OFF,             //!< Power down device & clear configuration
    DS_AVGCNF,          //!< Set average cycles
    DS_CALIB,           //!< Load calibration data
    DS_CLRINT,          //!< Clear status flags
    DS_CRUN,            //!< Power-up device
    DS_READY,           //!< Device is ready
    DS_REQ,             //!< Measurement request
    DS_POOLING,         //!< Check device status
    DS_READ_DATA        //!< Read data
};

static volatile uint8_t dev_state;  //!< Device state indicator
static hts221_reg_t registers;      //!< Device registers
static void (*data_rdy_call)(void); //!< Data ready callback
static float rH;                    //!< Actual relative humidity
static float temperature;           //!< Actual temperature
static float tg_t;                  //!< Calibration line T'
static float tg_rh;                 //!< Calibration line rH'
static tim_t *timer;                //!< Pooling timer handler
/*------------------------------  I2C COMMUNICATION  ------------------------------*/

#define I2C_BUFF_SIZE   16              //!< I2C communication buffer size
static uint8_t i2c_buff[I2C_BUFF_SIZE]; //!< Data buffer for i2c communication

/**
 *  Check status register.
 *  Calling by timer every 0.1s.
 * @param r reps
 */
static void i2c_pooling(uint16_t r);

/**
 * I2C data reception callback.
 */
static void hts221_i2c_callback(uint8_t err) {
    switch (dev_state) {
        case DS_OFF:    // -> DS_AVGCNF
            dev_state++;
            break;

        case DS_AVGCNF: // -> DS_CALIB
            dev_state++;
            break;

        case DS_CALIB:
            // Calibration data received.
            // Registers content is stored in i2c_buff array.
            // Copy data to register structure.
            registers.h0_rh_x2 = i2c_buff[0];
            registers.h1_rh_x2 = i2c_buff[1];
            registers.t0_degC_x8 = i2c_buff[2];
            registers.t1_degC_x8 = i2c_buff[3];
            registers.h0_t0_out = (int16_t)(i2c_buff[6] | ((uint16_t) i2c_buff[7] << 8));
            registers.h1_t0_out = (int16_t)(i2c_buff[10] | ((uint16_t) i2c_buff[11] << 8));
            registers.t0_out = (int16_t)(i2c_buff[12] | ((uint16_t) i2c_buff[13] << 8));
            registers.t1_out = (int16_t)(i2c_buff[14] | ((uint16_t) i2c_buff[15] << 8));
            registers.t1_t0_msb = i2c_buff[5];

            registers.t1_degC_x8 = ((uint16_t)(registers.t1_t0_msb & 0x0c) << 6) | registers.t1_degC_x8;
            registers.t0_degC_x8 = ((uint16_t)(registers.t1_t0_msb & 0x03) << 8) | registers.t0_degC_x8;
            // Calculate interpolation lines tangent
            tg_t = (registers.t0_degC_x8 - registers.t1_degC_x8) / 8.0 / (registers.t1_out - registers.t0_out);
            tg_rh = (registers.h1_rh_x2 - registers.h0_rh_x2) / 2.0 / (registers.h1_t0_out - registers.h0_t0_out);
            dev_state++; // -> DS_CLRINT
            break;

        case DS_CLRINT: // -> DS_CRUN
            dev_state++;
            break;

        case DS_CRUN:
            dev_state++; // -> DS_READY
            break;

        case DS_REQ:
            dev_state = DS_POOLING;
            // Start pooling
            timer = timer_start(i2c_pooling, 100, 0);
            break;

        case DS_POOLING:
            registers.stat_reg = i2c_buff[0];
            if ((registers.stat_reg & HTS221_STAT_DRDY) == HTS221_STAT_DRDY) {
                dev_state = DS_READ_DATA;
                // Read humidity and temperature
                i2c_buff[0] = HTS221_H_OUT_ADDR | HTS221_MULTI_RW;
                i2c_comm(HTS221_HW_ADDR, 1, i2c_buff, 4, i2c_buff, hts221_i2c_callback);
                // Stop pooling timer
                timer_remove(timer);
            }
            break;

        case DS_READ_DATA:
            // New data received
            // Copy data to local struct
            registers.h_out = (int16_t)(i2c_buff[0] | ((uint16_t) i2c_buff[1] << 8));
            registers.t_out = (int16_t)(i2c_buff[2] | ((uint16_t) i2c_buff[3] << 8));
            // Calculate actual temperature and humidity
            rH = tg_rh * (registers.h_out - registers.h0_t0_out) + registers.h0_rh_x2 / 2.0;
            temperature = tg_t * (registers.t1_out - registers.t_out) + registers.t1_degC_x8 / 8.0;
            dev_state = DS_READY;
            // Signalize data reception
            (*data_rdy_call)();
            break;
    }
}

static void i2c_pooling(uint16_t r) {
    i2c_buff[0] = HTS221_STAT_ADDR | HTS221_MULTI_RW;
    i2c_comm(HTS221_HW_ADDR, 1, i2c_buff, 1, i2c_buff, hts221_i2c_callback);
}
/*--------------------------------  INTERFACE  ---------------------------------------*/

void hts221_init(void (*callback)(void)) {
    data_rdy_call = callback;
    dev_state = DS_OFF;
    /*  Set CTRL_REG1 address and clear config registers  */
    i2c_buff[0] = HTS221_CTRL1_ADDR | HTS221_MULTI_RW;
    i2c_buff[1] = 0;
    i2c_buff[2] = 0;
    i2c_buff[3] = 0;
    // Queue i2c request
    i2c_comm(HTS221_HW_ADDR, 4, i2c_buff, 0, 0, hts221_i2c_callback);

    /*  Set AV_CONF register address, set max precision */
    i2c_buff[0] = HTS221_CTRL1_ADDR | HTS221_MULTI_RW;
    i2c_buff[1] = HTS221_AVCONF_T_256 | HTS221_AVCONF_H_512;
    // Queue i2c request
    i2c_comm(HTS221_HW_ADDR, 4, i2c_buff, 0, 0, hts221_i2c_callback);

    // Get calibration data from all registers
    // Set H0_RH_X2 address, enable multi read
    i2c_buff[0] = HTS221_H0_RH_X2_ADDR | HTS221_MULTI_RW;
    // Queue i2c request: reading 16 bytes
    i2c_comm(HTS221_HW_ADDR, 1, i2c_buff, 16, i2c_buff, hts221_i2c_callback);

    // Reading 0x29 & 0x2B registers clears interrupt
    i2c_buff[0] = HTS221_H_OUT_ADDR | HTS221_MULTI_RW;
    // Add to i2c queue: read 4 x uint8_t
    i2c_comm(HTS221_HW_ADDR, 1, i2c_buff, 4, i2c_buff, hts221_i2c_callback);

    // Setup control register 1
    i2c_buff[0] = HTS221_CTRL1_ADDR | HTS221_MULTI_RW;
    // Power up, output data rate = one shot
    registers.ctrl_reg1 = HTS221_CTRL1_PWR_UP | HTS221_CTRL1_RATE_OS;
    i2c_buff[1] = registers.ctrl_reg1;
    i2c_comm(HTS221_HW_ADDR, 2, i2c_buff, 0, 0, hts221_i2c_callback);
}

void hts221_measure(void) {
    if (dev_state == DS_READY) {
        dev_state = DS_REQ;
        // Request measurement
        i2c_buff[0] = HTS221_CTRL2_ADDR | HTS221_MULTI_RW;
        // One shot enable
        i2c_buff[1] = HTS221_CTRL2_OS_EN;
        i2c_comm(HTS221_HW_ADDR, 2, i2c_buff, 0, 0, hts221_i2c_callback);
    }
}

float hts221_get_rh(void) {
    return rH;
}

float hts221_get_temperature(void) {
    return temperature;
}