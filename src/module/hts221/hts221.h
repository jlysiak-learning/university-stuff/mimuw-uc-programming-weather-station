/**
 * @defgroup hts221 HTS221
 * @author Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 * HTS221 humidity & temperature sensor.
 * Hardware configuration:
 *  - STM32f411xx Nucleo board
 *  - KA-Nucleo-Weather shield
 */

/**
 * @file
 * @ingroup hts221
 */

#ifndef HTS221_H
#define HTS221_H

/**
 * Initialize device.
 * Require @ref i2c module initialization.
 * Function configures proper peripheral blocks, initializes
 * device structures and queues i2c config read/write requests.
 * @param callback data ready handler
 */
void hts221_init(void (*callback)(void));

/**
 * Start measurement.
 */
void hts221_measure(void);

/**
 * Get humidity.
 * @return relative humidity in %
 */
float hts221_get_rh(void);

/**
 * Get temperature.
 * @return temperature
 */
float hts221_get_temperature(void);

#endif /* HTS221_H */