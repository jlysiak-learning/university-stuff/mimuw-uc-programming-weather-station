/**
 * @file
 * @ingroup hts221
 * HTS221 configuration file
 */
#ifndef HTS221_CONF_H
#define HTS221_CONF_H

#include <stm32.h>

#define HTS221_HW_ADDR              0x5F            //!< I2C hardware address
/*--------------------------  REGISTERS ADDRESSES & REGISTERS' BITS  ----------------------------------*/

#define HTS221_AVCONF_ADDR          0x10    //!< Average cycles register, uint8_t
#define HTS221_CTRL1_ADDR           0x20    //!< Configuration register 1 address, uint8_t
#define HTS221_CTRL2_ADDR           0x21    //!< Configuration register 2 address, uint8_t
#define HTS221_CTRL3_ADDR           0x22    //!< Configuration register 3 address, uint8_t
#define HTS221_STAT_ADDR            0x27    //!< Status register address, uint8_t
#define HTS221_H0_RH_X2_ADDR        0x30    //!< H0 rH param address, uint8_t
#define HTS221_H1_RH_X2_ADDR        0x31    //!< H1 rH param address, uint8_t
#define HTS221_H0_T0_OUT_ADDR       0x36    //!< H0 T0 out param address, uint16_t
#define HTS221_H1_T0_OUT_ADDR       0x3A    //!< H1 T0 out param address, uint16_t
#define HTS221_H_OUT_ADDR           0x28    //!< result register address, uint16_t

#define HTS221_AVCONF_T_2           0x00    //!< 2 temperature average cycles, noise(RMS)[degC]: 0.08
#define HTS221_AVCONF_T_4           0x08    //!< 4 temperature average cycles, noise(RMS)[degC]: 0.05
#define HTS221_AVCONF_T_8           0x10    //!< 8 temperature average cycles, noise(RMS)[degC]: 0.04
#define HTS221_AVCONF_T_16          0x18    //!< (default after powerup) 16 temperature average cycles, noise(RMS)[degC]: 0.03
#define HTS221_AVCONF_T_32          0x20    //!< 32 temperature average cycles, noise(RMS)[degC]: 0.02
#define HTS221_AVCONF_T_64          0x28    //!< 64 temperature average cycles, noise(RMS)[degC]: 0.015
#define HTS221_AVCONF_T_128         0x30    //!< 128 temperature average cycles, noise(RMS)[degC]: 0.01
#define HTS221_AVCONF_T_256         0x38    //!< 256 temperature average cycles, noise(RMS)[degC]: 0.007

#define HTS221_AVCONF_H_4           0x00    //!< 4 humidity average cycles, noise(RMS)[%]: 0.4
#define HTS221_AVCONF_H_8           0x01    //!< 8 humidity average cycles, noise(RMS)[%]: 0.3
#define HTS221_AVCONF_H_16          0x02    //!< 16 humidity average cycles, noise(RMS)[%]: 0.2
#define HTS221_AVCONF_H_32          0x03    //!< (default after power up) 32 humidity average cycles, noise(RMS)[%]: 0.15
#define HTS221_AVCONF_H_64          0x04    //!< 64 humidity average cycles, noise(RMS)[%]: 0.1
#define HTS221_AVCONF_H_128         0x05    //!< 128 humidity average cycles, noise(RMS)[%]: 0.07
#define HTS221_AVCONF_H_256         0x06    //!< 256 humidity average cycles, noise(RMS)[%]: 0.05
#define HTS221_AVCONF_H_512         0x07    //!< 512 humidity average cycles, noise(RMS)[%]: 0.03

#define HTS221_CTRL1_PWR_UP         0x80    //!< Power down control
#define HTS221_CTRL1_BDU            0x04    //!< Block data update until read
#define HTS221_CTRL1_RATE_OS        0x00    //!< Set rate: one shot
#define HTS221_CTRL1_RATE_1HZ       0x01    //!< Set rate: 1Hz
#define HTS221_CTRL1_RATE_7HZ       0x02    //!< Set rate: 7Hz
#define HTS221_CTRL1_RATE_12_5HZ    0x03    //!< Set rate: 12.5Hz

#define HTS221_CTRL2_OS_EN          0x01    //!< One shot enable, run measurement

#define HTS221_CTRL3_INT_EN         0x04    //!< Enable Data Ready interrupt signal
#define HTS221_CTRL3_INT_PP_OD      0x40    //!< Output type, 0: PushPull, 1: OpenDrain
#define HTS221_CTRL3_INT_ACT_L      0x80    //!< Output type, 0: Active High, 1: Active Low

#define HTS221_STAT_T_DA            0x01    //!< Status register flag: T data available
#define HTS221_STAT_H_DA            0x02    //!< Status register flag: H data available
#define HTS221_STAT_DRDY            0x03    //!< All data available

#define HTS221_MULTI_RW             0x80    //!< Flag which allows to multi-byte read/write

#endif //HTS221_CONF_H
