#---------------------------------------------------------------------------------------------------------------
#------------------------------	PROJECT	----------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------
# Project name
PROJ_NAME = wea1

# Output directory
OUT_DIR = build

# Processor type
PROC = STM32F411xE

# All source files
SRC = startup_stm32.c main.c sbrk.c $(notdir $(shell find src/module src/utils src/core -name '*.c'))

# All object files 
OBJ = $(patsubst %.c,%.o,$(SRC))

# Source files directories
PATH_SRC_C = src src/$(shell find src -type d)

# Header files paths
PATH_INC = src src/$(shell find src -type d)

LINKER_SCRIPT = stm32f411re.lds
LINKER_PATH = src

#---------------------------------------------------------------------------------------------------------------
#------------------------------	COMPILER	----------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------
CC = arm-eabi-gcc
OBJCOPY = arm-eabi-objcopy

# uC compiler flags
FLAGS = -mthumb -mcpu=cortex-m4 -mfloat-abi=softfp -mfpu=fpv4-sp-d16 -D__FPU_USED=1 -D$(PROC) -DMAIN_CLOCK_MHZ=16

# Compiler include paths
INCLUDES = $(addprefix -I,$(PATH_INC))

# Compiler flags
CFLAGS = $(FLAGS) -Wall -g -O2 -ffunction-sections -fdata-sections $(INCLUDES)

# Linker flags
LDFLAGS = $(FLAGS) -Wl,--gc-sections -nostartfiles -L$(LINKER_PATH) -T$(LINKER_SCRIPT)

vpath %.c $(PATH_SRC_C)

TARGET = $(OUT_DIR)/$(PROJ_NAME)

#---------------------------------------------------------------------------------------------------------------
#------------------------------	TARGETS	----------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------
.SECONDARY: $(TARGET).elf $(OBJ)

all: directories $(TARGET).bin

#Make the Directories
directories:
	@echo "Make output directory..."
	@mkdir -p $(OUT_DIR)

%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $(OUT_DIR)/$@

%.elf: $(OBJ)
	$(CC) $(LDFLAGS) $(addprefix $(OUT_DIR)/,$^) -o $@

%.bin : %.elf
	$(OBJCOPY) $< $@ -O binary

clean :
	@echo "Cleaning..."
	@rm -rf build doc

prog: $(TARGET).bin
	./qfn4 $(TARGET).bin

doc:
	doxygen

flash: $(TARGET).bin
	st-flash write $< 0x8000000

test:
	@echo $(OBJ)
	@echo
	@echo $(PATH_INC)
.PHONY: clean directories doc
