## MIMUW - Microcontrollers programming
WEA1 - Simply weather station.

	Device displays on LCD screen actual weather parameters:
		- temperature (3 sources available)
		- pressure
		- relative humidity
		- light intensity
##### TODO (if free time will be available):
	- repair cmake file and integrate all with CMSIS & ARM toolchain
	- write own async lcd routines 
	- during developement one sensor was probably broken thus easier polling method was used (change to async)

##### Used hardware:

	- NUCLEO-F411RE STM32F4 evaluation board
	- KA-NUCLEO-Weather expansion shield
	- TFT 1.8" LCD display (ST7735S controller, 128x160, 18b color depth) 

##### Project structure:

	[.]                 - Makefile, Doxygen configuration
	|---[documents]     - Important documents, datasheets, etc.
	|---[doc]           - Doxygen documentation, available after build with "make doc" 
	|---[src]           - Sources, vectors, linker script
	    |---[config]    - Configuration files, stuff which is not a part of any module
	    |---[core]      - Core files, CMSIS registers layout, gpio config module 
	    |---[module]    - Modules folder
	    |   |---[xxx]   - Module sources, headers & configuration
	    |   |---[yyy]   - ...
	    |
	    |---[utils]     - Utilities, sources & headers
